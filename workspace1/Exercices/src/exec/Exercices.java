package exec;

import calc.Calculatrice;
import tools.Clavier;

public class Exercices {

	public static void main(String[] args) {
		Exercices.exercice1();
	}

	public static void exercice1() {
		System.out.println("*************************************");
		System.out.println("Addition deux entiers : ");
		int parametreX = Clavier.lireInt();
		int parametreY = Clavier.lireInt();
		int res = Calculatrice.addition(parametreX, parametreY);
		System.out.println("la somme = " + res);
	}
}

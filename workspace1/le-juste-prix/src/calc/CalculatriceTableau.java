package calc;

public class CalculatriceTableau {

	// M�thode addition �l�ments tableau
	public static int sommeElements(int[] tabInt) {
		int somme = 0;
		for (int i = 0; i < tabInt.length; i++) {
			somme = somme + tabInt[i];
		}
		return somme;

	}

	// M�thode plusPetitElement d'un tableau
	public static int plusPetitElement(int[] tabInt) {
//		int plusPetit = Integer.MAX_VALUE;
//		for (int elementTableau : tabInt) {
//			if (plusPetit > elementTableau) {
//				plusPetit = elementTableau;
//			}
//		}

		int plusPetit = tabInt[0];
		for (int i = 1; i < tabInt.length; i++) {
			if (plusPetit > tabInt[i]) {
				plusPetit = tabInt[i];
			}

		}
		return plusPetit;
	}

	// M�thode plusGrandElement d'un tableau
	public static int plusGrandElement(int[] tabInt) {

		int plusGrand = Integer.MIN_VALUE;
		for (int i = 1; i < tabInt.length; i++) {
			if (plusGrand <= tabInt[i]) {
				plusGrand = tabInt[i];
			}

		}
		return plusGrand;
	}

	// M�thode Somme des �l�ments de 2 tableaux
	public static long sommeElementsDeuxTableaux(int[] tabInt, int[] tabInt2) {
		return CalculatriceTableau.sommeElements(tabInt) + CalculatriceTableau.sommeElements(tabInt2);
	}

	// M�thode 9 triAscendant
	public static int[] triAscendant(int[] tabInt) {
		int[] tabTrie = new int[5];
		int temp = 0;
		for (int i = 0; i < tabInt.length; i++) {
			for (int j = (i + 1); j < tabInt.length; j++) {
				if (tabInt[i] > tabInt[j]) {
					temp = tabInt[j];
					tabInt[j] = tabInt[i];
					tabInt[i] = temp;
				}
			}
			tabTrie[i] = tabInt[i];
		}
		return tabTrie;
	}

	// EXO 14 M�thode conjonction 5 �l�ments d'un tab
	public static boolean conjonction(boolean[] tabBool) {
		boolean b = true;
		for (int i = 0; i < tabBool.length; i++) {
			if (!tabBool[i]) {
				b = false;
			}
		}
		return b;

	}

	// M�thode 15 triAscendantDeuxTableaux
//	public static int[] triAscendantDeuxTableaux(int[] tabInt, int[] tabInt2) {
//		int[] tabTri1 = triAscendant(tabInt);
//		int[] tabTri2 = triAscendant(tabInt2);
//
//		int[] tabsTrie = new int[tabTri1.length + tabTri2.length];
//		int temp = 0;
//		for (int i = 0; i < tabTri1.length; i++) {
//			for (int j = 0; j < tabTri2.length; j++) {
//
//				if (tabInt[i] > tabInt2[i]) {
//
//		}
//
//		return tabsTrie;
//
//	}

	// M�thode 16 nombreDElementsPair dans un tab
	public static int nombreDElementsPair(int[] tab) {
		int nombreDePair = 0;
		for (int i = 0; i < tab.length; i++) {
			if (tab[i] % 2 == 0) {
				nombreDePair++;
			}
		}

		return nombreDePair;
	}

	// M�thode 17 chercheSiUnElementExiste
	public static boolean chercheSiUnElementExiste(int param, int[] tab) {
		boolean existe = false;
		for (int i = 0; i < tab.length && !existe; i++) {
			if (param == tab[i]) {
				existe = true;
			}
		}
		return existe;
	}
//	//M�thode WEI exo 17
//	public static boolean chercheSiUnElementExiste(int a, int[] b) {
//		String s = Arrays.toString(b);
//		String c = Integer.toString(a);
//		return s.contains(c);
//	} (modifi�) 

	// M�thode exo 18 mettreZeroDansLesCasesAIndicesImpair
	public static int[] mettreZeroDansLesCasesAIndicesImpair(int[] tab) {
		int[] tabZero = new int[5];
		for (int i = 0; i < tab.length; i++) {
			if (tabZero[i] % 2 == 1) {
				tabZero[i] = 0;
			}
		}
		return tabZero;
	}

// M�thode exo 19 decalerLesElementsTableauDUneCase
	public static int[] decalerLesElementsTableauDUneCase(int[] tab) {
		int[] tabDecale = new int[tab.length];
		for (int i = 0; i < tab.length - 1; i++) {
			tabDecale[i + 1] = tab[i];
		}
		tabDecale[0] = tab[tab.length - 1];
		return tabDecale;
	}

//M�thode exo 19 Mohamed
//	public static int[] decalerLesElementsTableauDUneCase(int[] tab) {
//		int j = tab.length - 1;
//		for (int i = 0; i < j ; i++) {
//			tab[i] = tab[i] + tab[j];
//			tab[j] = tab[i] - tab[j];
//			tab[i] = tab[i] - tab[j];
//		}
//		return tab;
//	}

}
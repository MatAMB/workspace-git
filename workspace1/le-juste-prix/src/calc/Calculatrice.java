package calc;

public class Calculatrice {

	// M�thode addition
	public static int addition(int a, int b) {
		return a + b;
	}

	// M�thode soustraction
	public static int soustraction(int a, int b) {

		if (a < b) {
			return 0;
		}

		return a - b;
	}

	// M�thode multiplication
	public static int multiplication(int a, int b) {

		if (a == 0 || b == 0) {
			return 0;
		}

		return a * b;
	}

	// M�thode MAX
	public static int max(int a, int b) {
		if (a > b) {
			return a;
		}
		return b;
	}

	// M�thode qui retourne le signe d'un entier
	public static char signe(int a) {
		if (a >= 0) {
			return '+';
		}
		return '-';

	}

	// EXO 10 M�thode Factorielle
	public static long factorielle(int a) {

		// avec le for
//		long facto = 1;
//		for (int i = 1; i <= a; i++) {
//			facto *= i;
//		}
//		return facto;

		// avec le while
		int i = 1;
		long facto = 1;
		while (i <= a) {
			facto *= i;
			i++;
		}
		return facto;
	}

	// EXO 11 M�thode Conjonction boole
	public static boolean conjonction(boolean a, boolean b) {
		return a && b;
	}

	//EXO 12 M�thode Conjonction boole
	public static boolean disjonction(boolean a, boolean b) {
		return a || b;
	}

	// EXO 13 M�thode Conjonction boole
	public static boolean negation(boolean a) {
		return !a;
	}

}

package tools;

import java.util.Random;

import calc.CalculatriceTableau;

//import java.util.Arrays;

public class MotsUtils {

//	public static String inverser(String str) {
//		char[] tabChar = new char[str.length()];
//		String mot = "";
//		for (int i = 0; i < str.length(); i++) {
////			System.out.println("-------- i :" + i);
////			System.out.println("str.charAt(i) :<" + str.charAt(i) + ">");
//			tabChar[str.length() - 1 - i] = str.charAt(i);
////			System.out.println("- tabChar[i] :<" + tabChar[i] + ">");
//
//		}
//		for (int j = str.length() - 1; j == 0; j++) {
//			mot = "";
//			String lettre = Arrays.toString(tabChar);
//			System.out.println("<" + lettre + ">");
//			mot = mot + lettre;
//		}
//		System.out.println("<" + mot + ">");
////		String motInverse = Arrays.toString(tabChar);
////		System.out.println("<"+motInverse+">");
//		return mot;
//	}

	public static String inverser(String str) {
		String mot = "";
		for (int i = str.length() - 1; i >= 0; i--) {
			mot += str.charAt(i);
		}
		return mot;
	}

	// M�thode EXO21 caracteresCommuns
	public static String caracteresCommuns(String s1, String s2) {
		String communs = "";
		for (int i = 0; i < s1.length() - 1; i++) {
			for (int j = 0; j < s2.length() - 1; j++) {
				char premierChar = s1.charAt(i);

				// Fcontion indexOf
				boolean existe = communs.indexOf(premierChar) != -1;

				if (s1.charAt(i) == s2.charAt(j) && !existe) {
					communs = communs + s2.charAt(j);
				}
			}
		}
		return communs;
	}

	// M�thode EXO22 estUnPalindrome
	public static boolean estUnPalindrome(String mot) {
		boolean estUnPalindrome = true;
		int j = mot.length() - 1;
		for (int i = 0; i < j; i++) {
			if (mot.charAt(i) == mot.charAt(j)) {
				j--;
			} else {
				estUnPalindrome = false;
			}
		}
		return estUnPalindrome;
	}

	// M�thode EXO25 sommeChiffresDansMot
	public static long sommeChiffresDansMot(String mot) {
		long sommeDesChiffres = 0;
		for (int i = 0; i < mot.length() - 1; i++) {
			if (Character.isDigit(mot.charAt(i))) {
				String digit = Character.toString(mot.charAt(i));
//				sommeDesChiffres += Integer.parseInt(mot.charAt(i)+"");
				sommeDesChiffres += Integer.parseInt(digit);

				/*
				 * d += Character.getNumericValue(str.charAt(i));// on recupere la valeur
				 * numerique du caractere
				 */
			}

		}
		return sommeDesChiffres;
	}

	// M�thode Somme chiffres int
	public static int sommeChiffresMotInt(String mot) {
		int sommeDesChiffres = 0;
		for (int i = 0; i <= mot.length() - 1; i++) {
			String chiffre = Character.toString(mot.charAt(i));
			sommeDesChiffres += Integer.parseInt(chiffre);
		}
		return sommeDesChiffres;
	}

	// M�thode EXO28 devinerAlgo
	public static String devinerAlgo(int param) {
//		String algo;

//		int a = param * 2;
//		String debut = Integer.toString(a);
//		int b = param - 1;
//		String fin = Integer.toString(b);
//
//		algo = debut + fin;
		return Integer.toString(2 * param) + Integer.toString(param - 1);
	}

	// M�thode Exo27 devinerAlgo2
	public static int conversionBinaire(String str) {
		int[] tabBinaire = new int[str.length()];
		String inverse = MotsUtils.inverser(str);
		for (int i = 0; i < inverse.length(); i++) {
			if (inverse.charAt(i) == '1') {
				tabBinaire[i] = (int) Math.pow(2, i);
			}
		}
		return CalculatriceTableau.sommeElements(tabBinaire);
	}

	// M�thode 26 sommeUnicodes
	public static long sommeUnicodes(String str) {
		long somme = 0;
		for (int i = 0; i < str.length(); i++) {
			somme += str.charAt(i);
		}
		return somme;
	}

//	* cr�er une methode qui prend deux parametres de type entier strictement
//    * positif et qui affiche ceci si parametres sont 3 et 4 alors affiche 5*7*9*11;
//    * si parametres sont 11 et 1 alors affiche 13; si parametres sont 22 et 3 alors
//    * affiche 23*25*27
//    */

	// M�thode 29 algoEtrange
	public static String algoEtrange(int a, int b) {
		String chaine = "";
		if (a % 2 == 0) {
			a--;
		}
		for (int i = 0; i < b; i++) {
			a = a + 2;
			chaine += Integer.toString(a) + "*";
		}
		return chaine.substring(0, chaine.length() - 1);
	}

//	si parametre est 4 alors affiche T4E4D0
//	si parametre est 0.51 alors affiche T6E0D6
//	si parametre est 1.1 alors affiche T2E1D1
//	si parametre est 123.1 alors affiche T7E6D1

//	 M�thode 30 algoBizarre T=somme de tous les chiffres E=somme des entiers, D = Somme des d�cimales
	public static String algoBizarre(float a) {
		String chaine = Float.toString(a);
		String chaine2 = "";
		String chaine3 = "";
		int total = 0;
		int totalEntiers = 0;
		int totalDecimales = 0;
		int index = chaine.indexOf('.');

		for (int i = 0; i < index; i++) {
			chaine2 += chaine.charAt(i);
			totalEntiers = MotsUtils.sommeChiffresMotInt(chaine2);
		}
		for (int i = index + 1; i < chaine.length(); i++) {
			chaine3 += chaine.charAt(i);
			totalDecimales = MotsUtils.sommeChiffresMotInt(chaine3);
			System.err.println(totalDecimales);
		}
		total = totalEntiers + totalDecimales;
		return "T" + total + "E" + totalEntiers + "D" + totalDecimales;
	}
//	si param�tre est 'iDp' alors affiche 9416
//	si param�tre est 'BaA' alors affiche 211
//	si param�tre est 'AfPa' alors affiche 16161
//	si param�tre est 'Za' alors affiche 261
	// M�thode 31 lettre --> position dans l'alphabet a ou A = 1; b=2 c=3...

	public static String algoAlphabet(String mot) {
		mot = mot.toLowerCase();
		String alphabet = "abcdefghijklmnopqrstuvwxyz";
		String resultat = "";
		for (int i = 0; i < mot.length(); i++) {
			resultat += alphabet.indexOf(mot.charAt(i)) + 1;
		}
		return resultat;
	}
// Exo32
//	si param�tre est aze3rt alors affiche azerrrt
//	si param�tre est 2abc1f alors affiche aabcf
//	si param�tre est xE3 alors affiche xE
//	si param�tre est a12b alors affiche abbbbbbbbbbbb
//	si param�tre est e4Ry alors affiche eRRRRy

	public static String alphaNum(String mot) {
		String result = "";
		String nombre = "";
		for (int i = 0; i < mot.length() - 1; i++) {
			if (Character.isDigit(mot.charAt(i))) {
				nombre += mot.charAt(i);

				for (int j = 0; j < Character.getNumericValue(mot.charAt(i)); j++) {
					result += mot.charAt(i + 1);
				}
			}

			result += mot.charAt(i);
		}
		return result;
	}

	// Exo33 Affichage des �toiles

	public static String affichageEtoiles(int a) {
		String res = "";
		String saut = "\n";
		for (int i = 0; i < a; i++) {
			for (int j = 0; j < a - i; j++) {
				res += "*";
			}
			res += saut;
		}
		return res;
	}

	// M�thode EXO23 afficherNombreOccurence
	public static void afficherNombreOccurence(String mot) {
		// Fonctionne mais affiche a=5 a=4 a=3....

		mot = mot.toLowerCase();
		int cpt = 1;
		String test = "";
		for (int i = 0; i < mot.length(); i++) {
			if (test.indexOf(mot.charAt(i)) == -1) {

				for (int j = i + 1; j < mot.length(); j++) {
					if (mot.charAt(i) == mot.charAt(j)) {
						cpt++;
						// Utiliser indexOf == -1
					}
				}
			}
			System.out.println(mot.charAt(i) + " : " + cpt);
			cpt = 1;
		}

	}
	
	
	
	
	
	
	

}

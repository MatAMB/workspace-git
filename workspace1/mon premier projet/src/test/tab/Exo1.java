package test.tab;

import java.util.Scanner;

public class Exo1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
//		- �crire un programme qui :
//			-- cr�e un tableau de caract�res de 5 cases.
//			-- l�initialise avec des lettres de votre nom et pr�nom ( pas de saisie, �crire en dure dans le code ) .
//			-- saisit un nombre et affiche la case � cet indice.

		char[] tabChar;
		tabChar = new char[5];

		tabChar[0] = 'M';
		tabChar[1] = 'a';
		tabChar[2] = 't';
		tabChar[3] = 'h';
		tabChar[4] = 'i';

	
		int a;
		int taille = tabChar.length - 1;
		do {
			System.out.println("Saisissez un indice entre 0 et " + taille);
			a = sc.nextInt();
		} while (a >= 0 || a < 5);
		System.out.println(tabChar[a]);

		sc.close();

	}

}

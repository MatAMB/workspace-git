package exo1;
import java.util.Scanner;

public class Exo1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		// �crire un programme qui demande la saisie de 2 nombres et affiche le nombre
		// le plus grand.

		int a;
		int b;

		System.out.println("Saisir A");
		a = sc.nextInt();

		System.out.println("Saisir B");
		b = sc.nextInt();

		if (a == b) {
			System.out.println("Les nombres sont �gaux");
		} else if (a > b) {
			System.out.println("Le nombre le plus grand est : " + a);
		} else {
			System.out.println("Le nombre le plus grand est : " + b);
		}

		sc.close();

	}

}

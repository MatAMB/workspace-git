import java.util.Scanner;

public class Exos {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		// Exo1 Saisir 2 nombres les mettre dans 2 variables et afficher

		/*
		 * System.out.println("Saisir A"); int a; a = sc.nextInt();
		 * 
		 * System.out.println("Saisir B"); int b; b = sc.nextInt();
		 * 
		 * System.err.println("La valeur de A est : " + a);
		 * System.err.println("La valeur de B est : " + b);
		 */

		// Exo2 2 nombres SWAP A et B

		/*
		 * int a; int b; int c = 0;
		 * 
		 * System.out.println("Saisir A"); a = sc.nextInt();
		 * 
		 * System.out.println("Saisir B"); b = sc.nextInt();
		 * 
		 * System.out.println("La valeur de A est : " + a);
		 * System.out.println("La valeur de B est : " + b);
		 * 
		 * c = a; a = b; b = c;
		 * 
		 * System.err.println("Apr�s le SWAP A vaut :" + a);
		 * System.err.println("Apr�s le SWAP B vaut :" + b);
		 */

		// Exo 3 Saisie 3 nombres affiche la moy + partie enti�re de la moyenne % somme

		int a;
		int b;
		int c;

		int moyenne = 0;
		int somme = 0;
		int result = 0;

		System.out.println("Saisir A");
		a = sc.nextInt();

		System.out.println("Saisir B");
		b = sc.nextInt();

		System.out.println("Saisir C");
		c = sc.nextInt();

		System.out.println("La valeur de A est : " + a);
		System.out.println("La valeur de B est : " + b);
		System.out.println("La valeur de C est : " + c);

		somme = a + b + c;
		moyenne = somme / 3;

		System.err.println("La moyenne est : " + moyenne);

		result = moyenne % somme;

		System.err.println("Le mod est : " + result);

	}

}

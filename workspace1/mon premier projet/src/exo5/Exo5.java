package exo5;
import java.util.Scanner;

public class Exo5 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
//		�crire un programme qui demande la saisie de 2 nombres positifs 
//		et affiche tous les nombres pairs entre ces 2 nombres.

		System.out.println("-----------------------------------------------------------------------");
		System.out.println("Afficher tous les nombres pairs entre deux positifs");
		System.out.println("-----------------------------------------------------------------------");

		int a;
		int b;
		int c;

		do {

			System.out.println("Saisir le premier nombre : ");
			a = sc.nextInt();
		} while (a == 0);

		do {

			System.out.println("Saisir le deuxi�me nombre : ");
			b = sc.nextInt();
		} while (b == 0);

		if (a > b) {
			c = a;
			a = b;
			b = c;

		}

		if (a % 2 == 0) {
			a += 2;
		} else {
			a++;
		}

		System.out.println("Les nombres pairs compris entre ces deux nombres sont : ");

		while (a < b) {
			System.out.print(a + " ");
			a += 2;
		}

		sc.close();
	}
}

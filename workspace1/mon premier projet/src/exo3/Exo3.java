package exo3;

public class Exo3 {

	public static void main(String[] args) {

//		�crire un programme qui affiche les 10 premiers nombres pairs en utilisant une boucle while.
//		refaire la m�me chose en utilisant une boucle for

		System.out.println("---------------------------------------------------------------------------");
		System.out.println("Afficher les 10 premiers nombres pairs");
		System.out.println("---------------------------------------------------------------------------");

//		Avec une boucle for

		int a = 2;
		for (int i = 0; i < 10; i++) {
			System.out.print(a + " ");
			a = a + 2;
		}
		
		System.out.println(" ");
		System.out.println("---------------------------------------------------------------------------");
		
//		Avec une boucle while

		int b = 2;
		int i = 0;

		while (i < 10) {
			System.out.print(b + " ");
			b = b + 2;
			i++;

		}

	}
}

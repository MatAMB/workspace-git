package exo7;
import java.util.Scanner;

public class Exo7 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
//		Un magasin de reprographie facture 0,10 E les dix premi�res photocopies, 0,09 E les vingt suivantes et 0,08 E au-del�.
//		�crivez un algorithme qui demande � l�utilisateur le nombre de photocopies effectu�es et qui affiche la facture correspondante.

		System.out.println("-------------------------------------------------------------------------------");
		System.out.println("La photocopieuse");
		System.out.println("-------------------------------------------------------------------------------");

		int copy;
		double facture = 0;

		System.out.println("Veuillez saisir le nombre de copies � effectuer : ");
		copy = sc.nextInt();

		if (copy < 10) {
			facture = copy * 0.1;
		} else if (copy < 30) {
			facture = 1 + (copy - 10) * 0.09;
		} else {
			facture = 2.8 + (copy - 30) * 0.08;
		}

		System.out.println("Total � payer : " + facture+" �");

		sc.close();
	}

}

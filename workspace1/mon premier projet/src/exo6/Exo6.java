package exo6;
import java.util.Scanner;

public class Exo6 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("-------------------------------------------------------------------------------");
		System.out.println("Saisir un nombre tant que valeur n'est pas comprise entre 2 bornes");
		System.out.println("-------------------------------------------------------------------------------");

		int a;
		int b;
		int c;
		int t = 0; // Variable tempon pour SWAP

		System.out.println("Saisir la premi�re borne : ");
		a = sc.nextInt();

		System.out.println("Saisir la deuxi�me borne : ");
		b = sc.nextInt();

		if (a > b) {
			t = a;
			a = b;
			b = t;
		}

		do {
			System.out.println("Saisir le nombre � tester : ");
			c = sc.nextInt();

		} while (c < a || c > b);

		System.out.println("OK");
	

		sc.close();
	}
}

package exo2;
import java.util.Scanner;

public class Exo2 {

	// �crire un programme qui saisie un nombre et affiche oui si le nombre est
	// pair, non sinon.

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int a;
		int mod;
		System.out.println("---------------------------------------------------------------");
		System.out.println("Pair ou impair ?");
		System.out.println("---------------------------------------------------------------");

		System.out.println("Saisir un nombre � tester : ");
		a = sc.nextInt();

//		if (a < 2) {
//			System.err.println("Erreur de saisie, saisir � nouveau : ");
// Essayer de g�rer les erreurs de saisie		

			mod = a % 2;

			// Contr�le du modulo
			System.out.println("Le modulo 2 est : " + mod);

			if (mod == 0) {
				System.out.println("Le nombre est Pair");

			} else {
				System.out.println("Le nombre est Impair");

			}

		
		sc.close();
	}
}

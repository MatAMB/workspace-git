package exec;

import tools.Clavier;
import tools.MotsUtils;

public class Main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Main.exerciceEtoiles();

	}

	public static void exerciceEtoiles() {
		System.out.println("*************************************");
		System.out.println("Affichage du triangle d'�toiles");
		System.out.println("*************************************");
		int param = Clavier.lireInt();
		String res = MotsUtils.affichageEtoiles(param);
		System.out.println("Le resultat pour " + param + " est : ");
		System.out.println(res);
	}

}